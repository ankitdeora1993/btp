import numpy as np
import cv2
import time

print "enter image number :"
num = input()

start = time.time()

img = cv2.imread("F:\\study8\\btp\\dataset\\my img\\"+str(num)+".jpg")
#img  = cv2.imread('F:\\study8\\btp\\dataset\\my img\\trainedImg\\T1.jpg')

mask = cv2.imread('F:\\study8\\btp\\dataset\\my img\\labelledImg\\L'+str(num)+'.jpg',0)
#mask = cv2.imread("F:\\study8\\btp\\dataset\\my img\\outputImg\\O"+str(num)+".jpg",0)

org_h,org_w = mask.shape

temp = np.zeros((org_h,org_w,3),np.uint8);
temp[:,:,2] = mask
overlayed = cv2.add(img,temp)

res = cv2.inpaint(img, mask, 20, cv2.INPAINT_TELEA)
vis = np.hstack([img,res])
cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\InpaintedImgTraining\\R'+str(num)+'.jpg',vis) 

done = time.time()
elapsed = done - start
print "elapsed time: ",elapsed

#cv2.imshow('input',img)
cv2.imshow('mask',mask)
cv2.imshow('result',res)
cv2.imshow('overlayed',overlayed)

cv2.waitKey(0);
cv2.destroyAllWindows();