import cv2
import numpy as np
import training
import myFunc
import time

area_offset = 120
maxCandidates = 50
thresholdVal = 100

#kernel_dilate = np.ones((i,i),np.uint8)
#kernel_erode = np.ones((j,j),np.uint8)
kernel_dilate = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))

#working 1,5,6,9,10,11,12,13,14,15

print "enter image number : "
testNum = input();

train_img  = cv2.imread('F:\\study8\\btp\\dataset\\my img\\trainedImg\\T13.jpg',0)
test_img = cv2.imread('F:\\study8\\btp\\dataset\\my img\\'+str(testNum)+'.jpg',0)
my_img = cv2.imread('F:\\study8\\btp\\dataset\\my img\\'+str(testNum)+'.jpg')
labelledImg = cv2.imread('F:\\study8\\btp\\dataset\\my img\\labelledImg\\L13.jpg',0)
labelledImg = myFunc.normalizedImg(labelledImg)

start = time.time()

#myFunc.DataToCSV(train_img,'train',True,labelledImg,area_offset,maxCandidates,thresholdVal) #comment it to avoid training
myFunc.DataToCSV(test_img,'test',False,labelledImg,area_offset,maxCandidates,thresholdVal)
training.trainData()              

outputImg = myFunc.outputImgFromCSV(test_img)
res = cv2.inpaint(my_img, outputImg, 20, cv2.INPAINT_NS)
vis = np.hstack([my_img,res])

done = time.time()
elapsed = done - start
print "elapsed time: ",elapsed

cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\outputImg\\O'+str(testNum)+'.jpg',outputImg) #change the output file name
cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\InpaintedImgTraining\\R'+str(testNum)+'.jpg',vis) 
cv2.imshow('inpainted',vis)
cv2.imshow('output',outputImg)

cv2.waitKey(0)
cv2.destroyAllWindows()