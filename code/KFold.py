import numpy as np
import pandas as pd
import csv

from sklearn.feature_extraction import DictVectorizer as DV
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import LinearSVC
from sklearn import svm
from sklearn import cross_validation as CV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier


train_file = 'F:\\study8\\btp\\dataset\\my img\\feature.csv'



print 'Reading ...'
data = pd.read_csv( train_file )

t_data = data['trueValue']
x_data = data['x']
y_data = data['y']

data.drop('trueValue', axis=1, inplace=True)
data.drop('x', axis=1, inplace=True)
data.drop('y', axis=1, inplace=True)

kf = CV.KFold(len(data), n_folds=4)


sum_score = []

for train_index, test_index in kf:
    x_test = x_data[test_index]
    y_test = y_data[test_index]
    t_test = t_data[test_index]
    test = data.iloc[test_index]
    x_train = x_data[train_index]
    y_train = y_data[train_index]
    t_train = t_data[train_index]
    train = data.iloc[train_index]



	# numerical
    f_num_train = train.as_matrix()
    f_num_test = test.as_matrix()

	# complete x
##	f_train = np.hstack((f_num_train, vec_f_cat_train ))
##	f_test = np.hstack((f_num_test, vec_f_cat_test ))


    print 'Learning classifier...'
    clf = svm.SVC(kernel='linear', C=1).fit(f_num_train, t_train)
    score = clf.score(f_num_test,t_test)
    sum_score.append(score)
##    clf = OneVsOneClassifier(LinearSVC(random_state=0))
##	clf = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0,max_depth=1, random_state=0)
##	clf.fit(f_train,t_train)
##	out_test = clf.predict(f_test)
print sum_score
print np.sum(sum_score)/len(sum_score)









