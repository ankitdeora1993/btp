import cv2
import numpy as np
import myFunc
import time

##good ones : 1,13,20,24,31

area_offset = 120
maxCandidates = 30
thresholdVal = 100

print "enter image number : "
testNum = input();

myimg = cv2.imread('F:\\study8\\btp\\dataset\\my img\\'+str(testNum)+'.jpg')
test_img = cv2.imread('F:\\study8\\btp\\dataset\\my img\\'+str(testNum)+'.jpg',0)
labelled = cv2.imread('F:\\study8\\btp\\dataset\\my img\\labelledImg\\L'+str(testNum)+'.jpg',0)

gabor_filters = myFunc.build_filters()
texture_img = myFunc.process(test_img, gabor_filters)
houghImg,shapeCount =  myFunc.shapeMap(test_img,7)

start = time.time()
    
color_map, color_levels, color_points = myFunc.featureMapsFromCenterSurround(test_img,area_offset,maxCandidates,thresholdVal)
texture_map, texture_levels, texture_points = myFunc.featureMapsFromCenterSurround(texture_img,area_offset,maxCandidates,thresholdVal)
shape_map = myFunc.normalizedImg(houghImg)

color_map = color_map*255
texture_map = texture_map*255
shape_map = shape_map*255

result = cv2.bitwise_or(color_map,texture_map)
result = cv2.bitwise_or(result,shape_map)
common = cv2.bitwise_and(labelled,result)
temp_c = common.copy()

temp_res = result.copy()
r_contours, r_hierarchy = cv2.findContours(temp_res,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
r_drops_area = np.sum([cv2.contourArea(x) for x in r_contours])

temp_labelled = labelled.copy()
contours, hierarchy = cv2.findContours(temp_labelled,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
drops_area = np.sum([cv2.contourArea(x) for x in contours])

c_contours, c_hierarchy = cv2.findContours(common,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
c_drops_area = np.sum([cv2.contourArea(x) for x in c_contours])

print  "count:",r_drops_area/400, " % of area detected:", (c_drops_area*100)/drops_area

inpainted = cv2.inpaint(myimg, result,20, cv2.INPAINT_TELEA)
vis = np.hstack([myimg,inpainted])
cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\InpaintedImgNoTraining\\R'+str(testNum)+'.jpg',vis) 
cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\InpaintedImgNoTraining\\R_'+str(testNum)+'.jpg',inpainted)

done = time.time()
elapsed = done - start
print "elapsed time: ",elapsed


cv2.imshow('inpainted',vis)
cv2.imshow('res',result)
#cv2.imshow('common',temp_c)
#cv2.imshow('t_img',color_map)
#cv2.imshow('c_img',texture_map)
#cv2.imshow('s_img',shape_map)
#cv2.imshow('testimg',test_img)

cv2.waitKey(0)
cv2.destroyAllWindows()