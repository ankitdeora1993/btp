import numpy as np
import pandas as pd
import csv
from sklearn import svm

def trainData():
    train_file = 'F:\\study8\\btp\\dataset\\my img\\csvfiles\\train_feature.csv'
    test_file = 'F:\\study8\\btp\\dataset\\my img\\csvfiles\\test_feature.csv'

    print 'Reading ...'
    train = pd.read_csv( train_file )
    test = pd.read_csv( test_file )

    t_train = train['trueValue']
    x_train = train['x']
    y_train = train['y']
    x_test = test['x']
    y_test = test['y']

    train.drop('trueValue', axis=1, inplace=True)
    train.drop('x', axis=1, inplace=True)
    train.drop('y', axis=1, inplace=True)
    test.drop('x', axis=1, inplace=True)
    test.drop('y', axis=1, inplace=True)

    # numerical
    f_num_train = train.as_matrix()
    f_num_test = test.as_matrix()

    print 'Learning classifier...'
    clf = svm.SVC(kernel='linear', C=1)
    clf.fit(f_num_train, t_train)
    out_test = clf.predict(f_num_test)

    output = pd.DataFrame(out_test)

    print 'Learning completed'
    output.to_csv('F:\\study8\\btp\\dataset\\my img\\csvfiles\\prediction.csv',index=None)







