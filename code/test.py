import numpy as np
import cv2
    
img = cv2.imread("F:\\study8\\btp\\dataset\\my img\\31.jpg",0)
tempImg = img.copy()

hough_img = np.zeros_like(tempImg)
circles = cv2.HoughCircles(tempImg,cv2.cv.CV_HOUGH_GRADIENT,2,30,param1=50,param2=20,minRadius=1,maxRadius=20)
print "shapes count:", len(circles[0])

if circles is not None:
    circles = np.uint16(np.around(circles))
    for i in circles[0,:]:
        cv2.circle(hough_img,(i[0],i[1]),i[2]+3,255,-1)


#cv2.imshow('img',img)
cv2.imshow('shapes',hough_img)

cv2.waitKey(0)
cv2.destroyAllWindows()

